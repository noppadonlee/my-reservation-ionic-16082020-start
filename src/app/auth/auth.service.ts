import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _isLogin = false;
  private _userId = 'test';

  constructor() { }

  get isLogin() {
    return this._isLogin
  }

  get userId() {
    return this._userId;
  }

  login() {
    this._isLogin = true;
  }

  logout() {
    this._isLogin = false;
  }
}
